<?php

require 'vendor/autoload.php';

use App\WS\ToneAnalyser;
use Nahid\JsonQ\Jsonq;

$tone = new ToneAnalyser();
$output = $tone->api("Watson is a question-answering computer system capable of answering questions posed in natural language, developed in IBM's DeepQA project by a research team led by principal investigator David Ferrucci. Watson was named after IBM's founder and first CEO, industrialist Thomas J. Watson.The computer system was initially developed to answer questions on the quiz show Jeopardy! and, in 2011, the Watson computer system competed on Jeopardy! against legendary champions Brad Rutter and Ken Jennings, winning the first place prize of $1 million.");
//echo '<pre>';
$json = json_encode($output->document_tone);
//echo $json;//$tone->parse($output->document_tone);

$jsonFile = 'data/data_hotels.json';
$q = new Jsonq($jsonFile);
$res = $q->from('hotels')
    ->where('name', 'contains', 'Hotel')
    ->get();

//$res = $q->from('hotels..features')->get();
echo '<pre>';
var_dump($res);
