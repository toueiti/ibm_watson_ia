<?php

require 'vendor/autoload.php';

use Nahid\JsonQ\Jsonq;
use App\WS\Util;
use App\WS\ToneAnalyser;

$_id = ($_GET['id'] != null) ? $_GET['id'] : '0000';
$jsonFile = 'data/data_hotels.json';
$q = new Jsonq($jsonFile);

$res = $q->from('hotels')
        ->where('_id', '=', $_id)
        ->get();
$util = new Util();
$tone = new ToneAnalyser();

$hotel = (!empty($res)) ? array_pop($res) : [];
$reviews = [];
if (!empty($hotel)):
    $reviews = $tone->toneAll($hotel['Reviews']);
endif;
$data = [];
if (!empty($reviews)) {
    foreach ($reviews['statistic'] as $key => $val) {
        $row = [
            'name' => $key,
            'y' => ($reviews['nbstat'] != 0) ? $util->number_int($val / $reviews['nbstat']) : 0,
        ];
        $data [] = $row;
    }
}
echo json_encode($data);
?>
