<?php

require 'vendor/autoload.php';

use App\WS\Translator;

$trans = new Translator();

$input = $_POST['input'];
$model_id = $_POST['model_id'];

$json = $trans->api($input, $model_id);

echo $trans->parse($json);

