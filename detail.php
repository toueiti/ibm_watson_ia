<?php

require 'vendor/autoload.php';

use Nahid\JsonQ\Jsonq;
use App\WS\Util;
use App\WS\ToneAnalyser;

$_id = ($_GET['id'] != null) ? $_GET['id'] : '0000';
$jsonFile = 'data/data_hotels.json';
$q = new Jsonq($jsonFile);

$res = $q->from('hotels')
        ->where('_id', '=', $_id)
        ->get();
$util = new Util();
$tone = new ToneAnalyser();
//echo '<pre>';
//print_r(array_pop($res));
//die;
$hotel = (!empty($res)) ? array_pop($res) : [];
if (!empty($hotel)):
    ?>
<div class="bg-white" style="padding: 10px 20px;">
    <div>
        <a style="float: right" class="x-close" href="#">X</a>
        <h5 class="card-title"><?= $hotel['name'] ?></h5>
        <h2>&#36; <?= $hotel['price'] ?></h2>
        <h3 class="text-warning">
                <?= $util->renderStarts(intval($hotel['CLASS'])); ?>
        </h3>
        <p class="card-text">
            <i class="fa fa-map-marker-alt"></i><br>
            <small class="text-muted">
                    <?= $hotel['location'] ?>
            </small>
        </p>
            <?php foreach ($hotel['images'] as $image): ?>
        <img src="<?= $image ?>" alt="hôtel" class="img-thumbnail" style="width: 200px; height: 200px">
            <?php endforeach; ?>
        <div style="height: 20px"></div>
        <div class="row">
            <div class="col-md-8 offset-2">
                <div class="card" id="chart-container">
                    <div id="chart1"></div>
                </div>
            </div>
        </div>
        <div style="height: 20px"></div>
        <div class="card">
            <div class="card-body bg-light">
            <?php $reviews = $tone->toneAll($hotel['Reviews']); ?>
                <div class="row">
                <?php foreach ($reviews['statistic'] as $key => $val): 
                    $color = $tone->getColor($key);
                    ?>
                    <div class="col text-<?= $color ?>">
                            <?= $key ?>
                    </div> 
                <?php endforeach; ?>
                </div>
                <div class="row">            
                <?php
                foreach ($reviews['statistic'] as $key => $val):
                    $color = $tone->getColor($key);
                    ?>
                    <div class="col">
                        <span class="badge badge-pill badge-<?= $color ?>">
                            <?php if($reviews['nbstat']): ?>
                                <?= $util->number_percent($val / $reviews['nbstat']) ?>
                            <?php else : ?> 
                            0
                            <?php endif;?>
                        </span>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div style="height: 20px"></div>
            <?php foreach ($reviews['reviews'] as $comment): ?>
        <div class="card" style="margin: 10px;">
            <div class="card-body" comment-text>
                <div class="card-text">
                    <?= $comment['output'] ?>
                </div>
                <div  style="float: right">
                    <input type="hidden" class="trans_in" value="en">
                    <div class="text_in" style="display: none"><?= $comment['output'] ?></div>
                    <select class="trans_out">
                        <option selected="" value="fr">fr</option>
                        <option value="it">it</option>
                        <option value="es">es</option>
                        <option value="de">de</option>
                        <option value="ar">ar</option>
                        <option value="ja">ja</option>
                    </select>
                    <button trans-text class="btn btn-light btn-sm">
                        Translate
                    </button>
                    <button origin-text class="btn btn-light btn-sm" style="display: none">
                        Origin
                    </button>
                </div>
            </div>
        </div>
            <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>
