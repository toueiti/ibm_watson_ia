<?php

require 'vendor/autoload.php';

use App\WS\Chatbot;
use Nahid\JsonQ\Jsonq;
use App\WS\Util;

$input = ($_POST['input'] != null) ? $_POST['input'] : '';

$bot = new Chatbot();
$util = new Util();
$output = $bot->api($input);
$feats = [];
$props = [];
if (strpos($output, '000') === 0) {
    $tab = explode('000', $output);
    $props = explode(',', $tab[1]);
    $props = $util->arrayUcfirst($props);
    //print_r($props);die;
    
    $jsonFile = 'data/data_hotels.json';
    $q = new Jsonq($jsonFile);
    $result = $q->from('hotels')
            ->get();
    $res = [];
    $res [] = $result[rand(1, 10)];
    $res [] = $result[rand(1, 10)];
    /*foreach ($result as $line) {
        if ($util->isListInArray($props, $line['Property'])) {
            $res [] = $line;
        }
    }*/
    $i = 0;$out='';
    foreach ($res as $item) {
        $out .= $item['name'] . '<br>';
        $i++;
        if ($i == 3) {
            break;
        }
    }
    echo '<p class="card-text text-success">'
    . '<i class="fa fa-comment msg text-primary"></i>I recommend for you these</p> - ';
    echo '<p class="card-text text-primary">'
    . $out . '</p>';
} else {
    echo '<p class="card-text text-primary">'
    . '<i class="fa fa-comment msg text-primary"></i>'
    . $output . '</p>';
}