<?php

require 'vendor/autoload.php';

use Nahid\JsonQ\Jsonq;
use App\WS\Util;

$keyword = ($_GET['keyword'] != null) ? $_GET['keyword'] : '';
$props = ($_GET['props'] != null) ? explode(',', $_GET['props']) : [];
$feats = ($_GET['feats'] != null) ? explode(',', $_GET['feats']) : [];
$jsonFile = 'data/data_hotels.json';
$q = new Jsonq($jsonFile);
if ($keyword != '') {
    $result = $q->from('hotels')
            ->where('name', 'contains', $keyword)
            ->get();
} else {
    $result = $q->from('hotels')
            ->get();
}
$util = new Util();
$res = [];
foreach ($result as $line) {
    if($util->isListInArray($props, $line['Property']) &&
            ($util->isListInArray($feats, $line['features']))){
        $res [] = $line;
    }
}

foreach ($res as $item):
    ?>
<div class="card mb-3">
    <div class="row no-gutters">
        <div class="col-md-4">
            <img src="<?= $item['images'][0] ?>" class="card-img" alt="hôtel">
        </div>
        <div class="col-md-8">
            <div class="card-body offset-1">
                <h5 class="card-title">
                        <?= $item['name'] ?>
                </h5>
                <h2>$ <?= $item['price'] ?></h2>
                <h3 class="text-warning">
                        <?= $util->renderStarts(intval($item['CLASS'])); ?>
                </h3>
                <p class="card-text">
                    <i class="fa fa-map-marker-alt"></i>
                    <small class="text-muted">
                            <?= $item['location'] ?>
                    </small>
                </p>
                <p class="card-text">
                    <?php foreach ($item['Property'] as $prop):?>
                    <?= $prop ?> &nbsp;
                    <?php endforeach;?>
                </p>
                <p class="card-text">
                    <a hotel-detail href="detail.php?id=<?= $item ['_id'] ?>" class="btn btn-primary">Show more</a>
                </p>
            </div>
            <div class="resume-tone" data-id="<?= $item ['_id'] ?>">                
            </div>
        </div>
    </div>
</div>
    <?php
endforeach;
?>
