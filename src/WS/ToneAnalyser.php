<?php

namespace App\WS;

use Nahid\JsonQ\Jsonq;

class ToneAnalyser {

    const API_KEY = 'FcEWBT2upZZ853XSzdJHBtxA5xXnpowfz4avT6cNuxJS';
    const API_URL = 'https://gateway-lon.watsonplatform.net/tone-analyzer/api/v3/tone?version=2017-09-21';

    public $tabColor = [
        'Joy' => 'success',
        'Confident' => 'primary',
        'Tentative' => 'info',
        'Analytical' => 'secondary',
        'Sadness' => 'dark',
        'Fear' => 'warning',
        'Anger' => 'danger',
    ];

    public function api($input) {
        $ch = curl_init();
        $param = [
            "text" => $input,
        ];
        curl_setopt($ch, CURLOPT_URL, self::API_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($param));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, 'apikey' . ':' . self::API_KEY);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        $output = json_decode($result);
        //print_r($output);die;
        return $output;
    }

    public function parse($tab) {
        $doc_tones = [];
        if (property_exists($tab, 'document_tone')) {
            if (property_exists($tab->document_tone, 'tones')) {
                $doc_tones = $tab->document_tone->tones;
            }
        }
        if (property_exists($tab, 'sentences_tone')) {
            $sentenses = $tab->sentences_tone;
        } else {
            $sentenses = [];
        }
        $result = [
            'doc' => $doc_tones,
            'sentences' => $sentenses,
        ];
        return $result;
    }

    public function strFormat($input, $statistic) {
        $output = $input;
        $json = $this->api($input);
        $res = $this->parse($json);
        if (!empty($res['sentences'])) {
            foreach ($res['sentences'] as $item) {
                //$item = array_pop($res['sentences']);
                $elmt = array_pop($item->tones);
                if (is_object($elmt)) {
                    if (property_exists($elmt, 'tone_name')) {
                        $color = $this->getColor($elmt->tone_name);
                        $rep = '<span class="text-' . $color . '"><strong>' . $item->text . '</strong></span>';
                        $output = str_replace($item->text, $rep, $output);
                    }
                }
            }
        }
        foreach ($res['doc'] as $item) {
            $statistic [$item->tone_name] += $item->score;
        }
        return [
            'out' => [
                'output' => $output,
                'input' => $input
            ],
            'statistic' => $statistic
        ];
    }

    public function toneAll($comments) {
        $statistic = [
            'Joy' => 0,
            'Confident' => 0,
            'Tentative' => 0,
            'Analytical' => 0,
            'Sadness' => 0,
            'Fear' => 0,
            'Anger' => 0,
        ];
        $reviews = [];
        foreach ($comments as $comment) {
            $res = $this->strFormat($comment, $statistic);
            $statistic = $res['statistic'];
            $reviews [] = $res['out'];
        }
        $nbstat = 0;
        foreach ($statistic as $item) {
            $nbstat += $item;
        }
        return [
            'reviews' => $reviews,
            'statistic' => $statistic,
            'nbstat' => $nbstat,
        ];
    }

    public function getColor($sentence) {
        return $this->tabColor[$sentence];
    }

}
