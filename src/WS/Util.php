<?php

namespace App\WS;

class Util {

    public function renderStarts($nb) {
        $str = '';
        for ($i = 0; $i < $nb; $i++) {
            $str .= ' <i class="fa fa-star yellow"></i> ';
        }
        return $str;
    }

    function number_percent($val) {
        return number_format($val * 100, 2) . '%';
    }

    function number_int($val) {
        return number_format($val * 100, 0);
    }
    
    public function isListInArray($list, $array) {
        foreach ($list as $item){
            if(!in_array($item, $array)){
                return false;
            }
        }
        return true;
    }
    
    public function arrayUcfirst($array) {
        $resutl =[];
        foreach ($array as $val){
            $resutl[] = ucfirst($val);
        }
        return $resutl;
    }

}
