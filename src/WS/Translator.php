<?php

namespace App\WS;

class Translator {

    public function api($input, $model_id) {
        $ch = curl_init();
        $param = [
            "text" => $input,
            "model_id" => $model_id,
        ];
        curl_setopt($ch, CURLOPT_URL, 'https://gateway-lon.watsonplatform.net/language-translator/api/v3/translate?version=2018-05-01');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($param));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, 'apikey' . ':' . 'o1eS3LsfZNLw3NtH6xXXB7m1NeyV73rrr6kOk5rN72o4');

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }

    function parse($json) {
        $result = "";
        $tab = json_decode($json);
        if (property_exists($tab, "translations")) {
            $tab = $tab->translations;
            if (is_array($tab)) {
                foreach ($tab as $item) {
                    $result .= $item->translation;
                }
            }
        }
        return $result;
    }

}
