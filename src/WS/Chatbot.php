<?php

namespace App\WS;

class Chatbot {

    public function api($input) {
        $param = [
            "input" => [
                "text" => $input,
            ]
        ];
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://gateway-wdc.watsonplatform.net/assistant/api/v1/workspaces/f43c7644-b424-4153-a14e-59661f59ad63/message?version=2019-02-28'); //replace with your workspace_id
        //curl_setopt($ch, CURLOPT_URL, 'https://gateway-lon.watsonplatform.net/assistant/api/v1/workspaces/6576ca40-cd52-418a-95d3-09e83a7c867e/message?version=2019-02-28'); //replace with your workspace_id
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($param));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, 'apikey' . ':' . 'QfzF3PqjzbNGRMdcQYBvUPohCsMqRIa1yf4Fi1S5to5B');
        //curl_setopt($ch, CURLOPT_USERPWD, 'apikey' . ':' . 'aq1WJRdSWqqywSP6mzCNtpxWgjhstGF347V39pGmdZpv');

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        //return $result;
        return $this->parse($result);
    }

    function parse($json) {
        $text = '';
        $tab = json_decode($json);
        if (property_exists($tab, 'output')) {
            $tab = $tab->output;
            if (property_exists($tab, 'generic')) {
                $tab = $tab->generic;
                if (is_array($tab)) {
                    foreach ($tab as $item) {
                        if (property_exists($item, 'text')) {
                            $text .= $item->text . '<br>';
                        }
                    }
                }
            }
        }
        return $text;
    }

}
