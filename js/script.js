var property_list = new Array();
var feature_list = new Array();
$(function () {
    $.fn.scrollDown = function () {
        let el = $(this)
        el.scrollTop(el[0].scrollHeight)
    }
    h = $(window).height();
    w = $(window).width();
    $('.go-chatbot').css('top', h - ($('.go-chatbot').height()) + 'px');
    $('.go-chatbot').css('left', w - ($('.go-chatbot').width() + 30) + 'px');
    $.LoadingOverlay("show");
    $.ajax({
        url: 'search.php',
        type: 'get',
        data: {keyword: ''},
        success: function (result) {
            $('#result-list').html(result);
            $.LoadingOverlay("hide");
            //resumeTone();
        }
    });

    $(document).on('click', '#bot-on', function () {
        card = $('#bot-content');
        $(this).hide();
        card.show();
        $('.go-chatbot').css('top', h - ($('.go-chatbot').height()) + 'px');
        $('.go-chatbot').css('left', w - ($('.go-chatbot').width() + 30) + 'px');
    });

    $(document).on('click', '#bot-send', function () {
        card = $('#bot-body');
        text_in = $('#bot-text').val();
        $('#bot-text').val('');
        msg_in = '<p class="card-text text-secondary">';
        msg_in = msg_in + '<i class="fa fa-comment msg text-secondary"></i>';
        msg_in = msg_in + text_in + '</p>';
        card.append(msg_in);
        card.LoadingOverlay("show");
        $.ajax({
            url: 'bot.php',
            type: 'post',
            data: {input: text_in},
            success: function (result) {
                card.append(result);
                card.scrollDown();
                card.LoadingOverlay("hide");
            }
        });
    });
    
    $('.ch-prop').change(function(){
        v = $(this).val();
        if($(this).is(':checked')){
            property_list.push(v);
        } else {
            property_list.splice( $.inArray(v, property_list), 1 );;
        }
        filter_search();
    });
    
    $('.ch-fea').change(function(){
        v = $(this).val();
        if($(this).is(':checked')){
            feature_list.push(v);
        } else {
            feature_list.splice( $.inArray(v, feature_list), 1 );;
        }
        filter_search();
    });
    
    $(document).on('keyup', '#bot-text', function (e) {
        e.preventDefault();
        card = $('#bot-body');
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            text_in = $(this).val();
            $(this).val('');
            msg_in = '<p class="card-text text-secondary">';
            msg_in = msg_in + '<i class="fa fa-comment msg text-secondary"></i>';
            msg_in = msg_in + text_in + '</p>';
            card.append(msg_in);
            card.LoadingOverlay("show");
            $.ajax({
                url: 'bot.php',
                type: 'post',
                data: {input: text_in},
                success: function (result) {
                    card.append(result);
                    card.scrollDown();
                    card.LoadingOverlay("hide");
                }
            });
        }
    });

    $('.close-icon').on('click', function () {
        $('#bot-content').hide();
        $('#bot-on').show();
        $('.go-chatbot').css('top', h - ($('.go-chatbot').height()) + 'px');
        $('.go-chatbot').css('left', w - ($('.go-chatbot').width() + 30) + 'px');
    })

    $(document).on('click', '[trans-text]', function () {
        wrapper = $(this).closest('[comment-text]');
        comment = wrapper.find('.card-text');
        text_in = wrapper.find('.text_in');
        origin_text = wrapper.find('[origin-text]');
        comment.LoadingOverlay("show");
        lang_in = wrapper.find('.trans_in');
        lang_out = wrapper.find('.trans_out');
        $.ajax({
            url: 'trans.php',
            type: 'post',
            data: {input: text_in.text(), model_id: 'en-' + lang_out.val()},
            success: function (result) {
                comment.html(result);
                origin_text.show();
                comment.LoadingOverlay("hide");
            }
        });
    });

    $(document).on('click', '[origin-text]', function () {
        wrapper = $(this).closest('[comment-text]');
        comment = wrapper.find('.card-text');
        comment.LoadingOverlay("show");
        text_in = wrapper.find('.text_in');
        comment.html(text_in.html());
        comment.LoadingOverlay("hide");
        $(this).hide();
    });

    $(document).on('click', '.x-close', function () {
        $.LoadingOverlay("show");
        $.ajax({
            url: 'search.php',
            type: 'get',
            data: {keyword: ''},
            success: function (result) {
                $('#result-list').html(result);
                $.LoadingOverlay("hide");
            }
        });
    });

    $(document).on('keyup', '[search-input]', function () {
        keyword = $(this).val();
        $.LoadingOverlay("show");
        $.ajax({
            url: 'search.php',
            type: 'get',
            data: {keyword: keyword},
            success: function (result) {
                $('#result-list').html(result);
                $.LoadingOverlay("hide");
            }
        });
    });

    $(document).on('click', '[hotel-detail]', function (e) {
        e.preventDefault();
        url = $(this).attr('href');
        id = $(this).data('id');
        $.LoadingOverlay("show");
        $.ajax({
            url: url,
            type: 'get',
            success: function (result) {
                $('#result-list').html(result);
                $.LoadingOverlay("hide");
                load_chart(id);
            }
        });
    });

    Highcharts.setOptions({
        colors: ['#28A745', '#007BFF', '#17A2B8', '#6C757D', '#343A40', '#FFC107', '#DC3545', '#FFF263', '#6AF9C4']
    });
});

function resumeTone() {
    $('.resume-tone').each(function () {
        id = $(this).data('id');
        $(this).LoadingOverlay("show");
        $.ajax({
            url: 'statistic.php?id=' + id,
            type: 'get',
            success: function (result) {
                $(this).html(result);
                $(this).LoadingOverlay("hide");
            }
        });
    });
}

function filter_search(){
    keyword = $('[search-input]').val();
    props = property_list.join();
    feats = feature_list.join();
    $.LoadingOverlay("show");
    $.ajax({
        url: 'filter.php',
        type: 'get',
        data: {keyword: keyword, props:props, feats: feats},
        success: function (result) {
            $('#result-list').html(result);
            $.LoadingOverlay("hide");
        }
    });
}

function load_chart(id){
    $('#chart-container').LoadingOverlay("show");
    $.ajax({
        url: 'statistic.php?id='+id,
        type: 'get',
        success: function (result) {
            var myArr = $.parseJSON(result);
            data = new Array();
            $.each(myArr, function (idx, obj) {
                item = {
                    name:obj.name, 
                    y: parseInt(obj.y)
                };
                data.push(item);
            });
            statistic_chart(data);
            $('#chart-container').LoadingOverlay("hide");
        }
    });
}

function statistic_chart(data){
    Highcharts.chart('chart1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Tone Analyser result.'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
                name: 'Tone Analyser result',
                colorByPoint: true,
                data: data
            }]
    });
}
