<?php

require 'vendor/autoload.php';
use Nahid\JsonQ\Jsonq;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>IBM Watson WS Mini Project</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/default.css">
        <style>
            .go-chatbot {
                position: fixed;
                top:250px;
                z-index: 100;
            }
            .go-chatbot i {
                font-size: 300%;
                color: graytext;
                cursor: pointer;
            }
            
            .msg {
                font-size: 150% !important;
            }
            
            #bot-body {
                overflow: scroll;
            }
            
            .close-icon {
                cursor: pointer;
                float: right;
            }
            
            .separ {
                height: 8px;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="row">
                <div class="col3 offset-1"><h1>Hôtels </h1></div>
                <div class="col6 offset-1">                    
                </div>
            </div>
        </header>
        <section>
            <div class="go-chatbot">
                <i id="bot-on" class="fa fa-comment"></i>
                <div id="bot-content" class="card bg-light mb-3" style="width: 22rem; height: 22rem; display: none">
                    <div class="card-header">
                        Help
                        <span class="pull-right clickable close-icon" data-effect="fadeOut">
                            <i class="fa msg fa-times"></i>
                        </span>
                    </div>
                    <div id="bot-body" class="card-body">
                        Hello, I’m a demo customer care virtual assistant 
                        to show you the basics. I can help with directions 
                        to my store, hours of operation and booking
                        an in-store appointment
                    </div>
                    <div class="card-footer input-group">
                        <input id="bot-text" type="text" class="form-control">
                        <div class="input-group-prepend">
                            <button class="input-group-text" id="bot-send">
                                <i class="fa msg fa-arrow-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid bg-light row" style="padding: 25px">
                <div class="col-sm-3">                 
                    <div class="form-group offset-1">
                        <input type="text" search-input placeholder="search" class="form-control">
                    </div>
                    <div class="separ"></div>
                    <div class="card bg-light"> 
                        <div class="card-body"> 
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" class="property-list">
                                <input type="checkbox" class="custom-control-input property-item ch-prop" id="customCheck1" value="Parking">
                                <label class="custom-control-label" for="customCheck1">
                                    Parking
                                </label>
                            </div>
                            
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" class="property-list">
                                <input type="checkbox" class="custom-control-input property-item ch-prop" id="customCheck2" value="Pool">
                                <label class="custom-control-label" for="customCheck2">
                                    Pool
                                </label>
                            </div>
                            
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" class="property-list">
                                <input type="checkbox" class="custom-control-input property-item ch-prop" id="customCheck3" value="Wifi">
                                <label class="custom-control-label" for="customCheck3">
                                    Wifi
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="separ"></div>
                    <div class="card bg-light"> 
                        <div class="card-body"> 
                            <input type="hidden" class="property-list">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" class="property-list">
                                <input type="checkbox" class="custom-control-input property-item ch-fea" id="customCheck4" value="Suites">
                                <label class="custom-control-label" for="customCheck4">
                                    Suites
                                </label>
                            </div>
                            
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" class="property-list">
                                <input type="checkbox" class="custom-control-input property-item ch-fea" id="customCheck5" value="Family rooms">
                                <label class="custom-control-label" for="customCheck5">
                                    Family rooms
                                </label>
                            </div>
                            
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" class="property-list">
                                <input type="checkbox" class="custom-control-input property-item ch-fea" id="customCheck6" value="Flatscreen TV">
                                <label class="custom-control-label" for="customCheck6">
                                    Flatscreen TV
                                </label>
                            </div> 
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" class="property-list">
                                <input type="checkbox" class="custom-control-input property-item ch-fea" id="customCheck7" value="Non-smoking rooms">
                                <label class="custom-control-label" for="customCheck7">
                                    Non-smoking rooms
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div  class="col-sm-9">
                    <div id="result-list">                   
                    </div>
                </div>
            </div>
        </section>
        <footer>
            
        </footer>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/highlight.min.js"></script>
        <script src="js/loadingoverlay.min.js"></script>
        <script src="js/script.js"></script>
        <script>hljs.initHighlightingOnLoad();</script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="js/awesomefont.js" crossorigin="anonymous"></script>
    </body>
</html>
